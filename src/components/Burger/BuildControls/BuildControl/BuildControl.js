import React from "react"

import "./BuildControl.css"

const BuildControl = props => {

    return (
        <div className={"BuildControl"}>
            <div className={"Label"}>{props.label}</div>
            <button
                className={"Less"}
                disabled={props.disabled}
                onClick={()=> props.removeIngredient(props.type)}>LESS</button>
            <button
                className={"More"}
                onClick={()=> props.addIngredient(props.type)}>MORE</button>
        </div>
    )
}

export default BuildControl
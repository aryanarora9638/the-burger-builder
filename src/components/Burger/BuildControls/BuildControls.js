import React from "react"
import "./BuildControls.css"
import BuildControl from "./BuildControl/BuildControl";

const BuildControls = props => {

    const controls = [
        { label: 'Salad', type: 'salad' },
        { label: 'Bacon', type: 'bacon' },
        { label: 'Cheese', type: 'cheese' },
        { label: 'Meat', type: 'meat' }
    ]

    let burgerControls = controls.map((key,index) => {
        return (
            <BuildControl
                key={key.type}
                label={key.label}
                type={key.type}
                disabled={props.disabledInfo[key.type]}
                addIngredient={props.addIngredient}
                removeIngredient={props.removeIngredient}/>)
    })

    return (
        <div className={"BuildControls"}>
            <p>Current Price: <strong>{props.price.toFixed(2)}</strong></p>
            {burgerControls}
            <button
                onClick={props.ordered}
                disabled={!props.purchasable}>ORDER NOW</button>
        </div>
    )
}





export default BuildControls
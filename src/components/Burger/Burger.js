import React from "react"
import "./Burger.css"
import BurgerIngredients from "./BurgerIngredients/BurgerIngredients";

const Burger = props => {
    
    let ingredientsArray = Object.keys(props.ingredients)//makes object to array of keys
    let transformIngredients = ingredientsArray.map(key => {
        return (
            [...Array(props.ingredients[key])]
                .map((_, index) => {//creates an empty array of size (props.ingredients[key])
                    return (
                        <BurgerIngredients
                            key={key + index}
                            type={key}
                            qty={props.ingredients[key]}/>) //accessing the object with keys
                    })
        )
    })
    console.log(transformIngredients)

    let ingredientsAdded = false
    transformIngredients.forEach(key => {
        if(key.length !== 0){
            ingredientsAdded = true
        }
    })
     if(!ingredientsAdded){
         transformIngredients = <p>Please Start Adding Ingredients!!!</p>
     }


    return (
        <div className={"Burger"}>
            <BurgerIngredients type="bread-top" />
            {transformIngredients}
            <BurgerIngredients type="bread-bottom" />
        </div>
    )
}

export default Burger
//https://www.freecodecamp.org/news/https-medium-com-gladchinda-hacks-for-creating-javascript-arrays-a1b80cb372b/
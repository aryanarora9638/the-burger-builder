import React, {Component} from "react"
import Aux from "../../hoc/Aux/Aux";
import Burger from "../../components/Burger/Burger";
import BuildControls from "../../components/Burger/BuildControls/BuildControls"
import Modal from "../../components/UI/Modal/Modal"
import OrderSummary from "../../components/Burger/OrderSummary/OrderSummary";

class BurgerBuilder extends Component{
    constructor(props) {
        super(props)
        this.state = {
            ingredients : {
                salad : 0,
                cheese : 0,
                meat : 0,
                bacon : 0
            },
            ingredientPrice : {
                salad: 0.5,
                cheese: 0.4,
                meat: 1.3,
                bacon: 0.7
            },
            totalPrice : 4,
            purchasable : false,
            purchasing : false
        }
    }

    updatePurchaseState= (updatedIngredient) => {
        let sum = Object.keys(updatedIngredient)
            .map(key => {
            return updatedIngredient[key]
            })
            .reduce((a,b) => {
                return a+b
            },0)

        this.setState({
            purchasable : (sum > 0) ? true : false
         })
    }


    addIngredientHandler = (ingredientType) => {
        let oldQty = this.state.ingredients[ingredientType]
        let oldPrice = this.state.totalPrice
        let newQty = oldQty + 1
        let newPrice = oldPrice + this.state.ingredientPrice[ingredientType]

        const updatedIngredient = {
            ...this.state.ingredients
        }
        updatedIngredient[ingredientType] = newQty

        this.setState({
            ingredients : updatedIngredient,
            totalPrice : newPrice
        })
        this.updatePurchaseState(updatedIngredient)
    }


    removeIngredientHandler = (ingredientType) => {
        let oldQty = this.state.ingredients[ingredientType]
        if(oldQty < 1){
            return
        }
        let oldPrice = this.state.totalPrice
        let newQty = oldQty - 1
        let newPrice = oldPrice - this.state.ingredientPrice[ingredientType]

        const updatedIngredient = {
            ...this.state.ingredients
        }
        updatedIngredient[ingredientType] = newQty

        this.setState({
            ingredients : updatedIngredient,
            totalPrice : newPrice
        })
        this.updatePurchaseState(updatedIngredient)
    }

    disabledInfoHandler = () => {
        let disabledInfo = {
            ...this.state.ingredients
        }
        Object.keys(disabledInfo).map(key => {
            if(disabledInfo[key] === 0){
                disabledInfo[key] = true
            }
            else {
                disabledInfo[key] = false
            }
            return null
        })
        return disabledInfo
    }

    disabledInfoHandler = () => {
        let disabledInfo = {
            ...this.state.ingredients
        }
        Object.keys(disabledInfo).map(key => {
            if(disabledInfo[key] === 0){
                disabledInfo[key] = true
            }
            else {
                disabledInfo[key] = false
            }
            return null
        })
        return disabledInfo
    }

    purchaseHandler = () => {
        this.setState({purchasing: true});
    }

    purchaseCancelHandler = () => {
        this.setState({purchasing: false});
    }

    purchaseContinueHandler = () => {
        alert('You continue!');
        this.setState({purchasing: false});
    }



    render() {

        return (
            <Aux>
                <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
                    <OrderSummary
                        ingredients={this.state.ingredients}
                        purchaseCancelled={this.purchaseCancelHandler}
                        purchaseContinued={this.purchaseContinueHandler} />
                </Modal>
                <Burger
                    ingredients={this.state.ingredients} />
                <BuildControls
                    addIngredient={this.addIngredientHandler}
                    removeIngredient={this.removeIngredientHandler}
                    disabledInfo={this.disabledInfoHandler()} //execute it
                    purchasable={this.state.purchasable}
                    ordered={this.purchaseHandler}
                    price={this.state.totalPrice}/>
            </Aux>
        )
    }
}

export default BurgerBuilder